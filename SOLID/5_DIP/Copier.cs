﻿namespace SOLID.DIP
{
    public class Copier
    {
        private readonly KeyboardReader keyboardReader;
        private readonly PrinterWriter printerWriter;

        public void Copy()
        {
            char c;
            while((c = keyboardReader.ReadKeyboard()) != 'q')
            {
                printerWriter.WriteToPrinter(c);
            }
        }
    }

    public class KeyboardReader
    {
        public char ReadKeyboard()
        {
            return 'a';
        }
    }

    public class PrinterWriter
    {
        public void WriteToPrinter(char c)
        {
            //writing to some printer
        }
    }
}
