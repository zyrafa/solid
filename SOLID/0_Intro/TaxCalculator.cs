﻿using System;

namespace SOLID._0_Intro
{
    public class TaxCalculator
    {
        public double CalculateTax(double amount)
        {
            return amount * 1.23;
        }
    }
}
