﻿namespace SOLID.LSP
{
    public class Rectangle
    {
        private Point topLeft;
        private double width;
        private double height;

        public virtual double Width
        {
            get { return width; }
            set { width = value; }
        }

        public virtual double Height
        {
            get { return height; }
            set { height = value; }
        }

        public double Area
        {
            get
            {
                return width * height;
            }
        }
    }
}
