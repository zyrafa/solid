﻿namespace SOLID.LSP
{
    public class Square : Rectangle
    {
        public override double Width
        {
            set
            {
                base.Width = value;
                base.Height = value;
            }
        }

        public override double Height
        {
            set
            {
                base.Height = value;
                base.Width = value;
            }
        }

        public void Foo(Rectangle r)
        {
            r.Width = 5;
            r.Height = 4;
            if (r.Area != 20)
                throw new System.Exception("Bad area!");
        }
    }
}
