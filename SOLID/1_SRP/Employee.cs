﻿namespace SOLID.SRP
{
    public class Employee
    {
        private readonly int Rate = 600;
        private readonly IDatabaseAccess databaseAccess;

        public double CalculatePay(int days)
        {
            return days * Rate * 1.18;
        }

        public void Save()
        {
            databaseAccess.Save(this);
        }
    }

    public interface IDatabaseAccess
    {
        void Save(Employee e);
    }
}
