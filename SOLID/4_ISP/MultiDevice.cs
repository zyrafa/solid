﻿using System;

namespace SOLID.ISP
{
    public interface IMultiDevice
    {
        void Print();
        void Scan();
        void Fax();
    }

    public class MultiDevice : IMultiDevice
    {
        public void Fax()
        {
            Console.WriteLine("I do faxing");
        }

        public void Print()
        {
            Console.WriteLine("I do printing");
        }

        public void Scan()
        {
            Console.WriteLine("I do scanning");
        }
    }
}
