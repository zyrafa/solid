﻿using System;

namespace SOLID.OCP
{
    interface IShape
    {
        string Type { get; }
    }

    class Circle : IShape
    {
        public string Type => "Circle";
        private double Radius { get; }
        private int CenterX { get; }
        private int CenterY { get; }

        public Circle(double radius, int x, int y)
        {
            Radius = radius;
            CenterX = x;
            CenterY = y;
        }

        public void DrawCircle() { }
    }

    class Square : IShape
    {
        public string Type => "Square";
        private double SideLength { get; }
        private int TopLeftX { get; }
        private int TopLeftY { get; }

        public Square(double sideLength, int x, int y)
        {
            SideLength = sideLength;
            TopLeftX = x;
            TopLeftY = y;
        }

        public void DrawSquare() { }
    }

    class ShapeDrawer
    {
        public void DrawShapes(IShape[] shapes)
        {
            foreach (var shape in shapes)
            {
                switch (shape.Type)
                {
                    case "Circle":
                        ((Circle)shape).DrawCircle();
                        break;

                    case "Square":
                        ((Square)shape).DrawSquare();
                        break;
                }
            }
        }
    }
}
