﻿namespace SOLID.Exercises.ISP
{
    public class Example
    {
        public void CoffeMachineExample()
        {
            var coffeeMachine = new CoffeeMachine();

            var customer = new Customer(coffeeMachine);
            var staffMember = new Staff(coffeeMachine);
        }
    }
}
