﻿using System;

namespace SOLID.Exercises
{
    public interface ICoffeeMaker
    {
        string SelectDrinkType();
        string SelectPortion();
        int SelectSugarAmount();
        void BrewCoffee();
    }

    public interface ICoffeeMakerMaintenance
    {
        void CleanMachine();
        void FillCoffeeSupply();
        void FillWaterSupply();
        void FillSugarSupply();
    }

    public class CoffeeMachine : ICoffeeMaker, ICoffeeMakerMaintenance
    {
        public void BrewCoffee()
        {
            throw new NotImplementedException();
        }

        public void CleanMachine()
        {
            throw new NotImplementedException();
        }

        public void FillCoffeeSupply()
        {
            throw new NotImplementedException();
        }

        public void FillSugarSupply()
        {
            throw new NotImplementedException();
        }

        public void FillWaterSupply()
        {
            throw new NotImplementedException();
        }

        public string SelectDrinkType()
        {
            throw new NotImplementedException();
        }

        public string SelectPortion()
        {
            throw new NotImplementedException();
        }

        public int SelectSugarAmount()
        {
            throw new NotImplementedException();
        }
    }

    public class Customer
    {
        private readonly ICoffeeMaker _coffeeMaker;
        public Customer(ICoffeeMaker coffeeMaker)
        {
            _coffeeMaker = coffeeMaker;
        }

        public void MakeCoffee()
        {
            _coffeeMaker.SelectDrinkType();
            _coffeeMaker.SelectPortion();
            _coffeeMaker.SelectSugarAmount();
            _coffeeMaker.BrewCoffee();
            //_coffeeMaker.CleanMachine() <- tak nie możemy zrobić, bo dostajemy do dyspozycji tylko funkcjonalności związane z robieniem kawy
        }
    }

    public class Staff
    {
        private readonly ICoffeeMakerMaintenance _coffeeMakerMaintenance;
        public Staff(ICoffeeMakerMaintenance coffeeMakerMaintenance)
        {
            _coffeeMakerMaintenance = coffeeMakerMaintenance;
        }

        public void CleanMachine()
        {
            _coffeeMakerMaintenance.FillCoffeeSupply();
            _coffeeMakerMaintenance.FillSugarSupply();
            _coffeeMakerMaintenance.FillWaterSupply();
            _coffeeMakerMaintenance.CleanMachine();
            //_coffeeMakerMaintenance.BrewCoffee() <- tak nie możemy zrobić, bo dostajemy do dyspozycji tylko funkcjonalności związane utrzymaniem automatu do kawy
        }
    }
}
