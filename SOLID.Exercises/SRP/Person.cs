﻿using System;

namespace SOLID.Exercises
{
    public class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
    }

    public interface IFormatter<T>
    {
        string Format(T objectToFormat);
    }

    public class JsonFormatter<T> : IFormatter<T>
    {
        public string Format(T objectToFormat)
        {
            return $"{objectToFormat} in JSON format";
        }
    }

    public class XmlFormatter<T> : IFormatter<T>
    {
        public string Format(T objectToFormat)
        {
            return $"{objectToFormat} in XML format";
        }
    }
}
