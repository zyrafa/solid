﻿using System;

namespace SOLID.Exercises.SRP
{
    public class Example
    {
        public void PersonFormatterExmaple()
        {
            var personJsonFormatter = new JsonFormatter<Person>();
            var person = new Person
            {
                DateOfBirth = new DateTime(2000, 12, 20),
                FirstName = "John",
                LastName = "Doe"
            };

            var formattedPerson = personJsonFormatter.Format(person);
        }
    }
}
