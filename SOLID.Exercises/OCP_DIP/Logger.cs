﻿using System;

namespace SOLID.Exercises
{
    public interface IFormatter
    {
        string Format(string message);
    }

    public class JsonFormatter : IFormatter
    {
        public string Format(string message)
        {
            return $"{message} in JSON";
        }
    }

    public class XmlFormatter : IFormatter
    {
        public string Format(string message)
        {
            return $"{message} in XML";
        }
    }

    public class SimpleFormatter : IFormatter
    {
        public string Format(string message)
        {
            return $"Just a {message}";
        }
    }

    public class Logger
    {
        private readonly IFormatter _formatter;

        public Logger(IFormatter formatter)
        {
            this._formatter = formatter;
        }

        public void Log(string message)
        {
            var formattedMessage = _formatter.Format(message);
            Console.WriteLine(formattedMessage);
        }
    }
}
