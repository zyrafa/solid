﻿namespace SOLID.Exercises.OCP_DIP
{
    public class Example
    {
        public void LoggerExample()
        {
            var jsonFormatter = new JsonFormatter();
            var xmlFormatter = new XmlFormatter();

            var jsonLogger = new Logger(jsonFormatter);
            var xmlLogger = new Logger(xmlFormatter);
        }
    }
}
