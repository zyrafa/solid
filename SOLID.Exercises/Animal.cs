﻿namespace SOLID.Exercises
{
    public abstract class Animal
    {
        public virtual string Name { get; set; }
    }

    public class Dog : Animal
    {
        private readonly string _name = "Burek";

        //tak jak rozmawialiśmy na zajęciach, w przypadku tej klasy trzeba zastanowić się, dlaczego chcemy miec psy z niezmiennym imieniem
        public override string Name
        {
            get { return _name; }
            set { }
        }
    }
}
